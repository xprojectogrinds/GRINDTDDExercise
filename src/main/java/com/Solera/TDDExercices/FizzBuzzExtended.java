package com.Solera.TDDExercices;

public class FizzBuzzExtended {
    public String dividedByThree(int number){
        number = number < 0 ? number *= (-1) : number;
        String numberChain = String.valueOf(number);
        int sumDigits = 0;
        int[] digits = new int[numberChain.length()];

        for (int i = 0; i<numberChain.length(); i++){
            char digitChar = numberChain.charAt(i);
            digits[i] = Character.getNumericValue(digitChar);
            sumDigits+=digits[i];
        }
        return sumDigits%3==0 ? "Es divisible entre 3" : "No es divisible entre 3";
    }
}
