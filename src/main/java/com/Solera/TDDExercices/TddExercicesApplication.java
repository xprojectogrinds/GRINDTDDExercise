package com.Solera.TDDExercices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TddExercicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TddExercicesApplication.class, args);
	}

}
