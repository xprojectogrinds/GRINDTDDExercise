package com.Solera.TDDExercices.FormValidatorRules;

public class ChainOnlyNumber implements IRuleValidator{

    @Override
    public boolean validate(String chain) {
        try {
            Integer.parseInt(chain);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
