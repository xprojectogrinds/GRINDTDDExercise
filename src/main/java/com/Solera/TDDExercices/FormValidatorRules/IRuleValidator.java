package com.Solera.TDDExercices.FormValidatorRules;

public interface IRuleValidator {
    public boolean validate(String chain);
}
