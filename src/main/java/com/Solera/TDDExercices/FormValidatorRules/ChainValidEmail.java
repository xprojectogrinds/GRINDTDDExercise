package com.Solera.TDDExercices.FormValidatorRules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChainValidEmail implements IRuleValidator{

    @Override
    public boolean validate(String email) {
        String regexPattern = "^(?=.{1,64}@)[\\p{L}0-9_-]+(\\.[\\p{L}0-9_-]+)*@"
                + "[^-][\\p{L}0-9-]+(\\.[\\p{L}0-9-]+)*(\\.[\\p{L}]{2,})$";
        return Pattern.compile(regexPattern)
                .matcher(email)
                .matches();
    }
}