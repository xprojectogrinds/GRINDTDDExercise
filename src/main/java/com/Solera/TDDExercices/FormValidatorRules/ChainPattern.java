package com.Solera.TDDExercices.FormValidatorRules;

import java.util.regex.Pattern;

public class ChainPattern implements IRuleValidator{
    Pattern p;
    @Override
    public boolean validate(String chain) {
        return p.matcher(chain)
                .matches();
    }
    public ChainPattern(String regex) {
        this.p = Pattern.compile(regex);
    }
}