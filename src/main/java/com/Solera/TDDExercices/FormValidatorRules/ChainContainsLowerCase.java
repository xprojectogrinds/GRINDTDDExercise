package com.Solera.TDDExercices.FormValidatorRules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChainContainsLowerCase implements IRuleValidator {

    @Override
    public boolean validate(String chain) {
        Pattern p = Pattern.compile("[a-z]");
        Matcher m = p.matcher(chain);
        return m.find();
    }
}
