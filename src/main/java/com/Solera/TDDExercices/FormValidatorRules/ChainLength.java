package com.Solera.TDDExercices.FormValidatorRules;

public class ChainLength implements IRuleValidator{
    private int min;
    private int max;

    @Override
    public boolean validate(String chain) {
        int length = chain.length();
        if (length > this.max) return false;
        if (length < this.min) return false;
        return true;
    }

    public ChainLength(int min, int max) {
        this.min = min;
        this.max = max;
    }
}
