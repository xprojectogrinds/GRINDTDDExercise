package com.Solera.TDDExercices.FormValidatorRules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChainContainsUpperCase implements IRuleValidator{

    @Override
    public boolean validate(String chain) {
        Pattern p = Pattern.compile("[A-Z]");
        Matcher m = p.matcher(chain);
        return m.find();
    }
}
