package com.Solera.TDDExercices.FormValidatorRules;

import java.util.ArrayList;
import java.util.List;

public class ChainEqual implements IRuleValidator{
    private List<String> list;
    @Override
    public boolean validate(String chain) {
        for (String s: list) {
            if(chain.equals(s)) return true;
        }
        return false;
    }

    public ChainEqual(String word) {
        this.list = new ArrayList<>();
        this.list.add(word);
    }
    public ChainEqual(List<String> list) {
        this.list = list;
    }
}