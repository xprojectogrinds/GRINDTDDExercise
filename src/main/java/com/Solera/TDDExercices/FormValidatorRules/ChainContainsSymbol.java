package com.Solera.TDDExercices.FormValidatorRules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChainContainsSymbol implements IRuleValidator{

    @Override
    public boolean validate(String chain) {
        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(chain);
        return m.find();
    }
}
