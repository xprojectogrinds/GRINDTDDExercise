package com.Solera.TDDExercices.FormValidatorRules;

public class ChainOnlyLetter implements IRuleValidator{

    @Override
    public boolean validate(String chain) {
        return chain.matches("[a-zA-Z]+");
    }
}
