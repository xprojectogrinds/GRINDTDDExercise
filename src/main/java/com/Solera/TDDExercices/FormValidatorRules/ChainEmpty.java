package com.Solera.TDDExercices.FormValidatorRules;

public class ChainEmpty implements IRuleValidator{
    @Override
    public boolean validate(String chain) {
        return chain.isEmpty();
    }
}
