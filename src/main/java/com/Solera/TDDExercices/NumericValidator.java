package com.Solera.TDDExercices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumericValidator {

    public String numValidator(String[] numbers) {

        if (numbers == null) {
            return null;
        }

        ArrayList<Integer> positive = new ArrayList<>();
        ArrayList<Integer> negative = new ArrayList<>();
        ArrayList<Integer> prime = new ArrayList<>();
        ArrayList<Integer> pNumbers = new ArrayList<>(
                Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97)
        );


        for (String s : numbers) {

            Pattern pttern = Pattern.compile("^-?[0-9]+$");
            Matcher matcher = pttern.matcher(s);

            while (matcher.find()) {
                int number = Integer.parseInt(s);

                if (number >= 0) {
                    positive.add(number);
                    if (pNumbers.contains(number)) {
                        prime.add(number);
                    }
                } else {
                    negative.add(number);
                }

            }
        }

        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

        matrix.add(positive);
        matrix.add(negative);
        matrix.add(prime);

        System.out.println(matrix);
        return matrix.toString();
    }


}
