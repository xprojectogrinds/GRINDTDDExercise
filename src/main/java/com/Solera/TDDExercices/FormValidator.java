package com.Solera.TDDExercices;

import com.Solera.TDDExercices.FormValidatorRules.IRuleValidator;

import java.util.List;

public class FormValidator {
    public static boolean formValidator(List<String> list, List<IRuleValidator> ruleList) {
        for (String chain : list) {
            for (IRuleValidator rule: ruleList) {
                if(!rule.validate(chain)) return false;
            }
        }
        return true;
    }
}
