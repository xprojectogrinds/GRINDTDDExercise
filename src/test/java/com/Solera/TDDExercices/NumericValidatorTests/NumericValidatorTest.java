package com.Solera.TDDExercices.NumericValidatorTests;

import com.Solera.TDDExercices.NumericValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class NumericValidatorTest {
    @Test
    void primeValidate() {
        NumericValidator test = new NumericValidator();
        assertEquals( "[[31], [], [31]]" , test.numValidator(new String[]{"31"}));
    }

    @Test
    void negativeValidate() {
        NumericValidator test = new NumericValidator();
        assertEquals( "[[], [-31], []]" , test.numValidator(new String[]{"-31"}));
    }

    @Test
    void zeroValidate() {
        NumericValidator test = new NumericValidator();
        assertEquals( "[[0], [], []]", test.numValidator(new String[]{"0"}));
    }

    @Test
    void positiveValidate() {
        NumericValidator test = new NumericValidator();
        assertEquals( "[[1], [], []]", test.numValidator(new String[]{"1"}));
    }


    @Test
    void lettersValidate() {
        NumericValidator test = new NumericValidator();
        assertEquals( "[[], [], []]", test.numValidator(new String[]{"adad"}));
    }

    @Test
    void alphaNumericValidate() {
        NumericValidator test = new NumericValidator();
        assertEquals( "[[], [], []]", test.numValidator(new String[]{"ad222asd313"}));
    }

    @Test
    void hyphenValidate() {
        NumericValidator test = new NumericValidator();
        assertEquals( "[[], [], []]", test.numValidator(new String[]{"ad222a-sd313"}));
    }

    @Test
    void emptyStringValidate() {
        NumericValidator test = new NumericValidator();
        assertEquals( "[[], [], []]", test.numValidator(new String[]{""}));
    }

    @Test
    void nullValidate() {
        NumericValidator test = new NumericValidator();
        assertNull(test.numValidator(null));
    }
}
