package com.Solera.TDDExercices.FizzBuzzExtendedTests;

import com.Solera.TDDExercices.FizzBuzzExtended;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzExtendedTest {
    @Test
    public void testDividedByThreeWithValidInput() {
        FizzBuzzExtended fz = new FizzBuzzExtended();
        String result = fz.dividedByThree(334342843);
        Assertions.assertEquals("No es divisible entre 3", result);
    }
    @Test
    public void testDividedByThreeWithZero() {
        FizzBuzzExtended fz = new FizzBuzzExtended();
        String result = fz.dividedByThree(0);
        Assertions.assertEquals("Es divisible entre 3", result);
    }

    @Test
    public void testDividedByThreeWithNegativeInput() {
        FizzBuzzExtended fz = new FizzBuzzExtended();
        String result = fz.dividedByThree(-27);
        Assertions.assertEquals("Es divisible entre 3", result);
    }
}
