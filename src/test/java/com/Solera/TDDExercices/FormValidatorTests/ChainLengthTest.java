package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainLength;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChainLengthTest {
    @Test
    void chainLength() {
        String emptyString = "";
        String testString = "12345";

        int min = 0;
        int max = 0;
        Assertions.assertTrue(new ChainLength(min, max).validate(emptyString), String.format("Empty String with params min = %d, max = %d, should return true", min, max));

        max = 33;
        Assertions.assertTrue(new ChainLength(min, max).validate(emptyString), String.format("Empty String with params min = %d, max = %d, should return false", min, max));

        min = 1;
        Assertions.assertFalse(new ChainLength(min, max).validate(emptyString), String.format("Empty String with params min = %d, max = %d, should return false", min, max));
        min = 0;
        max = 5;
        Assertions.assertTrue(new ChainLength(min, max).validate(testString), String.format("String with length = %d, min = %d and max = %d, should return true", testString.length(), min, max));
        min = 5;
        max = 33;
        Assertions.assertTrue(new ChainLength(min, max).validate(testString), String.format("String with length = %d, min = %d and max = %d, should return true", testString.length(), min, max));
        max = 5;
        Assertions.assertTrue(new ChainLength(min, max).validate(testString), String.format("String with length = %d, min = %d and max = %d, should return true", testString.length(), min, max));
        min = 0;
        max = 4;
        Assertions.assertFalse(new ChainLength(min, max).validate(testString), String.format("String with length = %d, min = %d and max = %d, should return false", testString.length(), min, max));
        min = 6;
        max = 33;
        Assertions.assertFalse(new ChainLength(min, max).validate(testString), String.format("String with length = %d, min = %d and max = %d, should return false", testString.length(), min, max));
    }
}
