package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainOnlyLetter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChainOnlyLetterTest {
    @Test
    void chainOnlyLetter() {
        String emptyString = "";
        String numberString = "12345";
        String letterString = "abcde";
        String symbolString = "@|#~€";

        Assertions.assertFalse(new ChainOnlyLetter().validate(emptyString), "Empty String should return false");

        Assertions.assertTrue(new ChainOnlyLetter().validate(letterString), "Only letter String should return true");
        Assertions.assertFalse(new ChainOnlyLetter().validate(symbolString), "Only symbol String should return false");
        Assertions.assertFalse(new ChainOnlyLetter().validate(numberString), "Only number String should return false");

        Assertions.assertFalse(new ChainOnlyLetter().validate(letterString + symbolString), "Letter and Symbol String should return false");
        Assertions.assertFalse(new ChainOnlyLetter().validate(letterString + numberString), "Letter and Number String should return false");
    }
}
