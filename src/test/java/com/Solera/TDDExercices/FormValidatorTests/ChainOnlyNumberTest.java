package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainOnlyNumber;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChainOnlyNumberTest {
    @Test
    void chainOnlyNumber() {
        String emptyString = "";
        String numberString = "12345";
        String letterString = "abcde";
        String symbolString = "@|#~€";

        Assertions.assertFalse(new ChainOnlyNumber().validate(emptyString), "Empty String should return false");

        Assertions.assertFalse(new ChainOnlyNumber().validate(letterString), "Only letter String should return false");
        Assertions.assertFalse(new ChainOnlyNumber().validate(symbolString), "Only symbol String should return false");
        Assertions.assertTrue(new ChainOnlyNumber().validate(numberString), "Only number String should return false");

        Assertions.assertFalse(new ChainOnlyNumber().validate(numberString + symbolString), "Number and Symbol String should return false");
        Assertions.assertFalse(new ChainOnlyNumber().validate(numberString + letterString), "Number and Letter String should return false");
    }
}
