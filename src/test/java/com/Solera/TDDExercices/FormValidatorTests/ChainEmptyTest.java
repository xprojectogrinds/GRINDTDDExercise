package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainEmpty;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class ChainEmptyTest {
    @Test
    void chainEmpty() {
        String emptyString = "";
        String nonEmptyString = "asd123";
        Assertions.assertTrue(new ChainEmpty().validate(emptyString), "Empty String should return true");
        Assertions.assertFalse(new ChainEmpty().validate(nonEmptyString), "Non empty String should return true");
    }
}
