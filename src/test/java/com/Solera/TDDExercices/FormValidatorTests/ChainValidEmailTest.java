package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainValidEmail;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChainValidEmailTest {
    @Test
    void ChainValidEmail() {
        String emptyString = "";

        String validUsername = "name@";
        String invalidUsername = "name[@";

        String validDomain = "solera.";
        String invalidDomain = "solera{";

        String validSubdomain = "madrid.";
        String invalidSubdomain = "madrid@";

        String validExtension = "com";
        String invalidExtension = "()org";

        String validSimpleEmail = validUsername + validDomain + validExtension;
        String invalidUserSimpleEmail = invalidUsername + validDomain + validExtension;
        String invalidDomainSimpleEmail = validUsername + invalidDomain + validExtension;

        String validSubdomainEmail = validUsername + validDomain + validSubdomain + validExtension;
        String invalidSubdomainEmail = validUsername + validDomain + invalidSubdomain + validExtension;

        String invalidExtensionEmail = validUsername + validDomain + validSubdomain + invalidExtension;

        Assertions.assertFalse(new ChainValidEmail().validate(emptyString), "Empty Email should be invalid");

        Assertions.assertTrue(new ChainValidEmail().validate(validSimpleEmail), String.format("Email %s should be valid", validSimpleEmail));
        Assertions.assertFalse(new ChainValidEmail().validate(invalidUserSimpleEmail), String.format("Email %s, with username %s, should be invalid", invalidUserSimpleEmail, invalidUsername));
        Assertions.assertFalse(new ChainValidEmail().validate(invalidDomainSimpleEmail), String.format("Email %s, with domain %s, should be invalid", invalidDomainSimpleEmail, invalidDomain));

        Assertions.assertTrue(new ChainValidEmail().validate(validSubdomainEmail), String.format("Email %s should be valid", validSimpleEmail));
        Assertions.assertFalse(new ChainValidEmail().validate(invalidSubdomainEmail), String.format("Email %s, with subdomain %s,should be invalid", invalidSubdomainEmail, invalidSubdomain));

        Assertions.assertFalse(new ChainValidEmail().validate(invalidExtensionEmail), String.format("Email %s, with extension %s,should be invalid", invalidExtensionEmail, invalidExtension));
    }
}
