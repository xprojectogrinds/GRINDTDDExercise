package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ChainEqualTest {
    @Test
    void ChainEqual() {
        List<String> testList = new ArrayList<>();
        String stringTest0 = "asd12";
        String stringTest1 = "";
        String stringTest2 = "@|";
        String stringTest3 = " ";
        String invalidStringTest0 = "12asd";

        testList.add("asd12");
        testList.add("");
        testList.add("@|");
        testList.add(" ");

        Assertions.assertTrue(new ChainEqual(testList).validate(stringTest0), String.format("String %s is equal to String %s", stringTest0, testList.get(0)));
        Assertions.assertTrue(new ChainEqual(testList).validate(stringTest1), String.format("String %s is equal to String %s", stringTest1, testList.get(1)));
        Assertions.assertTrue(new ChainEqual(testList.get(2)).validate(stringTest2), String.format("String %s is equal to String %s", stringTest2, testList.get(2)));
        Assertions.assertTrue(new ChainEqual(testList.get(3)).validate(stringTest3), String.format("String %s is equal to String %s", stringTest3, testList.get(3)));

        Assertions.assertFalse(new ChainEqual(testList).validate(invalidStringTest0), String.format("String %s is not equal to any String inside the list", invalidStringTest0));
    }
}
