package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChainDateTest {
    @Test
    void chainDate() {
        String MMDDYY = "03/27/23";
        String DDMMYY = "27/03/23";
        String YYMMDD = "23/03/27";
        String DDMMYYYY = "23/03/2023";

        String dayError = "33/03/2023";
        String monthError = "23/33/2023";

        String leapYear = "29/02/2024";
        String notLeapYear = "29/02/2023";

        Assertions.assertFalse(new ChainDate().validate(MMDDYY), "Date format MMDDYY should be invalid");
        Assertions.assertFalse(new ChainDate().validate(DDMMYY), "Date format DDMMYY should be invalid");
        Assertions.assertFalse(new ChainDate().validate(YYMMDD), "Date format YYMMDD should be invalid");
        Assertions.assertTrue(new ChainDate().validate(DDMMYYYY), "Date format DDMMYYYY should be valid");

        Assertions.assertFalse(new ChainDate().validate(dayError), "Day should be in range");
        Assertions.assertFalse(new ChainDate().validate(monthError), "Month should be in range");

        Assertions.assertTrue(new ChainDate().validate(leapYear), "29/02 in a Leap Year should be valid");
        Assertions.assertFalse(new ChainDate().validate(notLeapYear), "29/02 in a non Leap Year should be invalid");
    }
}
