package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainContainsUpperCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChainContainsUpperCaseTest {
    @Test
    void chainContainsUpperCase() {
        String emptyString = "";
        String numberString = "12345";
        String lowerCaseString = "abcde";
        String upperCaseString = "ABCDE";
        String symbolString = "@|#~€";

        Assertions.assertFalse(new ChainContainsUpperCase().validate(emptyString), "Empty String should return false");

        Assertions.assertTrue(new ChainContainsUpperCase().validate(upperCaseString), "Upper Case String should return true");
        Assertions.assertFalse(new ChainContainsUpperCase().validate(lowerCaseString), "Lower Case String should return false");
        Assertions.assertFalse(new ChainContainsUpperCase().validate(numberString), "Only number String should return false");
        Assertions.assertFalse(new ChainContainsUpperCase().validate(symbolString), "Only symbol String should return false");

        Assertions.assertTrue(new ChainContainsUpperCase().validate(upperCaseString + lowerCaseString), "Upper Case and Lower Case String should return true");
        Assertions.assertTrue(new ChainContainsUpperCase().validate(upperCaseString + numberString), "Upper Case and Number String should return true");
    }
}
