package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainContainsSymbol;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChainContainsSymbolTest {
    @Test
    void chainContainsSymbol() {
        String emptyString = "";
        String numberString = "12345";
        String letterString = "abcde";
        String symbolString = "@|#~€";

        Assertions.assertFalse(new ChainContainsSymbol().validate(emptyString), "Empty String should return false");

        Assertions.assertTrue(new ChainContainsSymbol().validate(symbolString), "Only symbol String should return true");
        Assertions.assertFalse(new ChainContainsSymbol().validate(letterString), "Only letter String should return false");
        Assertions.assertFalse(new ChainContainsSymbol().validate(numberString), "Only number String should return false");

        Assertions.assertTrue(new ChainContainsSymbol().validate(symbolString + letterString), "Symbol and letter String should return true");
        Assertions.assertTrue(new ChainContainsSymbol().validate(symbolString + numberString), "Symbol and Number String should return true");
    }
}
