package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainContainsLowerCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChainContainsLowerCaseTest {
    @Test
    void chainContainsLowerCase() {
        String emptyString = "";
        String numberString = "12345";
        String lowerCaseString = "abcde";
        String upperCaseString = "ABCDE";
        String symbolString = "@|#~€";

        Assertions.assertFalse(new ChainContainsLowerCase().validate(emptyString), "Empty String should return false");

        Assertions.assertTrue(new ChainContainsLowerCase().validate(lowerCaseString), "Lower Case String should return true");
        Assertions.assertFalse(new ChainContainsLowerCase().validate(upperCaseString), "Upper Case String should return false");
        Assertions.assertFalse(new ChainContainsLowerCase().validate(numberString), "Only number String should return false");
        Assertions.assertFalse(new ChainContainsLowerCase().validate(symbolString), "Only symbol String should return false");

        Assertions.assertTrue(new ChainContainsLowerCase().validate(lowerCaseString + upperCaseString), "Lower Case and Upper Case String should return true");
        Assertions.assertTrue(new ChainContainsLowerCase().validate(lowerCaseString + numberString), "Lower Case and Number String should return true");
        Assertions.assertTrue(new ChainContainsLowerCase().validate(lowerCaseString + symbolString), "Lower Case and Symbol String should return true");
    }
}
