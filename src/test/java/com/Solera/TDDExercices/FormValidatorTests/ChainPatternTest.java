package com.Solera.TDDExercices.FormValidatorTests;

import com.Solera.TDDExercices.FormValidatorRules.ChainPattern;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class ChainPatternTest {
    @Test
    void ChainPattern() {
        String emailPattern = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
        String validEmail = "username@solera.com";
        String invalidEmail = "username)@solera.com";

        String phonePattern = "^\\d{9}$";
        String validPhone = "123456789";
        String invalidPhone = "12345678";

        String postalCodePattern = "^\\d{5}$";
        String validPostalCode = "12345";
        String invalidPostalCode = "123456";

        String urlPattern = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        String validUrl = "https://www.example.com";
        String invalidUrl = "www.example.com";

        Assertions.assertTrue(new ChainPattern(emailPattern).validate(validEmail), String.format("String %s must match pattern %s", validEmail, emailPattern));
        Assertions.assertFalse(new ChainPattern(emailPattern).validate(invalidEmail), String.format("String %s must not match pattern %s", invalidEmail, emailPattern));

        Assertions.assertTrue(new ChainPattern(phonePattern).validate(validPhone), String.format("String %s must match pattern %s", validPhone, phonePattern));
        Assertions.assertFalse(new ChainPattern(phonePattern).validate(invalidPhone), String.format("String %s must not match pattern %s", invalidPhone, phonePattern));

        Assertions.assertTrue(new ChainPattern(postalCodePattern).validate(validPostalCode), String.format("String %s must match pattern %s", validPostalCode, postalCodePattern));
        Assertions.assertFalse(new ChainPattern(postalCodePattern).validate(invalidPostalCode), String.format("String %s must not match pattern %s", invalidPostalCode, postalCodePattern));

        Assertions.assertTrue(new ChainPattern(urlPattern).validate(validUrl), String.format("String %s must match pattern %s", validUrl, urlPattern));
        Assertions.assertFalse(new ChainPattern(urlPattern).validate(invalidUrl), String.format("String %s must not match pattern %s", invalidUrl, urlPattern));
    }
}
