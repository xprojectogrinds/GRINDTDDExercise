package com.Solera.TDDExercices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestTddExercicesApplication {

	public static void main(String[] args) {
		SpringApplication.from(TddExercicesApplication::main).with(TestTddExercicesApplication.class).run(args);
	}

}
